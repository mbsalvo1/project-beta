from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.URLField(unique=True)

    def __str__(self):
        return self.vin 
    

class Technician(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    employee_number = models.PositiveSmallIntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100, null= True, blank=True)
    VIP_treatment = models.BooleanField(default=False)
    date_time = models.DateTimeField(null=True, blank=True)
    reason = models.TextField(null=True, blank=True)
    complete = models.BooleanField(default=False, null=True, blank=True)
    technician = models.ForeignKey(
        Technician, 
        related_name="appointment", 
        on_delete=models.CASCADE, 
        null=True, 
        blank=True
    )

    def __str__(self):
        return self.customer, self.date_time, self.reason, self.vin, self.VIP_treatment
    


