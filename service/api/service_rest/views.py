from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import AutomobileVO, Technician, Appointment
from .encoders import (TechnicianEncoder, AppointmentEncoder,)



@require_http_methods(["GET","POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()

        
        
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )

    else: #post
        try:
            content = json.loads(request.body)
            
            
            employee_number = content["technician"]
            technician = Technician.objects.get(employee_number=employee_number)
            content["technician"] = technician

            try:#for vip
                automobile = AutomobileVO.objects.get(vin=content["vin"])
                content["VIP_treatment"] = True
            except AutomobileVO.DoesNotExist:
                content["VIP_treatment"] = False
        except:
            response = JsonResponse(
                {"message": "Could not create appointment"}
            )
            response.status_code = 400
            return response
        
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
            )

        

    


@require_http_methods(["DELETE", "GET", "PUT"])
def api_appointment(request,pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment, 
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder, 
                safe=False, 
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    else: #put
        try:
            content=json.loads(request.body)
            appointment = Appointment.objects.get(id=pk)

            props = ["complete"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])

            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder, 
                safe=False, 
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET","POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )

    else: #post
        try:
            content = json.loads(request.body)
        except:
            response = JsonResponse({"message": "Could not create technician"})
            response.status_code = 404
            return response
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )
        



@require_http_methods(["DELETE"])
def api_technician(request,pk):

    try:
        technicians = Technician.objects.get(id=pk)
        technicians.delete()
        return JsonResponse(
            technicians,
            encoder=TechnicianEncoder,
            safe=False
        )
    except Technician.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response




#    try:
#             content = json.loads(request.body)
            
#             technician_id = content["technician_id"]
#             technician = Technician.objects.get(id=technician_id)
#             content["technician"] = technician

#             try:
#                 automobile = AutomobileVO.objects.get(vin=content["vin"])
#                 if automobile:
#                     content["vip_status"] = True
#             except AutomobileVO.DoesNotExist:
#                 pass

#             appointment = Appointment.objects.create(**content)
#             return JsonResponse(
#                 appointment,
#                 encoder=AppointmentEncoder,
#                 safe=False
#             )
#         except:
#             response = JsonResponse(
#                 {"message": "Could not create the appointment"}
#             )
            
#             response.status_code = 400
#             return response




    # try:
    #     content = json.loads(request.body)

    #     sale_id = content["sales_person"]
    #     seller_id = SalesPerson.objects.get(employee_number=sale_id)
    #     content["sales_person"] = seller_id
    # except:
    #     response = JsonResponse(
    #         {"message": "Could not create the sales person"}
    #     )
    #     response.status_code = 400
    #     return response
    # try:
    #     vin = content["automobile"]
    #     vehicle = AutomobileVO.objects.get(vin=vin)
    #     content["automobile"] = vehicle
    # except:
    #     response = JsonResponse(
    #         {"message": "Could not create the automobile"}
    #     )
    #     response.status_code = 400
    #     return response
    # try:
    #     customer_id = content["customer"]
    #     buyer_id = Customer.objects.get(id=customer_id)
    #     content["customer"] = buyer_id
    # except:
    #     response = JsonResponse(
    #         {"message": "Could not create the customer"}
    #     )
    #     response.status_code = 400
    #     return response

