from common.json import ModelEncoder
from .models import Technician, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id", 
        "name", 
        "employee_number",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id", 
        "vin",
        "VIP_treatment",
        "customer", 
        "date_time", 
        "reason", 
        "complete", 
        "technician"
    ]

    encoders = {
        "technician": TechnicianEncoder(),
    }
