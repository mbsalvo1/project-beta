from django.contrib import admin
from .models import SalesPerson, Customer, SalesLog, AutomobileVO

admin.site.register(SalesPerson)
admin.site.register(SalesLog)
admin.site.register(Customer)
admin.site.register(AutomobileVO)
