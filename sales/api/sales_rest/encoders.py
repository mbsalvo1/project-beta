from common.json import ModelEncoder

from .models import AutomobileVO, SalesLog, SalesPerson, Customer


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href"]



class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]


class SalesLogEncoder(ModelEncoder):
    model = SalesLog
    properties = [
        "id",
        "sales_person",
        "automobile",
        "purchase_price",
        "customer"
    ]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }
    # def get_extra_data(self, o):
    #     return {"automobile": o.automobile.vin}
