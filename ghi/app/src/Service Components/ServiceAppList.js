import React from "react";

class ServiceAppList extends React.Component{
    constructor() {
        super()
        this.state = { appointments: [] };
        this.OnDelete = this.OnDelete.bind(this);
        this.UpdateButton = this.UpdateButton.bind(this);
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ appointments: data.appointments });
        }
    }

    async OnDelete(id) {
        const url = `http://localhost:8080/api/appointments/${id}`;
        const fetchOptions = {
            method: "DELETE",
            headers: {
                'Content-Type': 'appointment/json',
            },
        };
        await fetch(url, fetchOptions);
        window.location.reload();
    }
    // component did update^ for next

    async UpdateButton(id) {
        const url = `http://localhost:8080/api/appointments/${id}`;
        const updateCar = JSON.stringify({complete: true})

        const fetchOptions = {
            method: "PUT",
            headers: {
                'Content-Type':'application/json',
            },
            body: updateCar
        };
        const response = await fetch(url, fetchOptions);
        if (response.ok) {
            window.location.reload(); 
        };
    }

    

        // if (response.ok) {
        //     this.setState({
        //         service: this.state.service.filter(service => service.id !== id)
        //     }); 
        // for filter
    


    render() {
    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technicians</th>
                        <th>Reason</th>
                        {/* <th>Vip</th> */}
                    </tr>
                </thead>
                <tbody>
                    {this.state.appointments.map(apt => {
                        return(
                        <tr key ={apt.id} >
                            <td>{apt.vin}</td>
                            <td>{apt.customer}</td>
                            <td>{apt.date_time}</td>
                            <td>{apt.date_time}</td>
                            <td>{apt.technician.name}</td>
                            <td>{apt.reason}</td>
                            {/* <td>{String(apt.VIP_treatment)}</td> */}
                            <td><button onClick={() => this.OnDelete(apt.id)} >cancel</button><button onClick={() => this.UpdateButton(apt.id)}>complete</button></td>
                            <td></td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        );
    }
}

export default ServiceAppList










// import React, { useState, useEffect } from "react";

// import React from "react";

// class ServiceAppList extends React.Component {
//     constructor() {
//         super()
//         this.state = {
//             technicians: []
//         };
//     }

//     async fetchAppointments(event) {
//         const curl = 'http://localhost:8080/api/appointments/';
//         const 
//     }

// }






