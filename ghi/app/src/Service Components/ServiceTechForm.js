import React from 'react';


class ServiceTechnicianForm extends React.Component{
    constructor() {
        super()
        this.state = {
            name: '',
            employee_number: '',
        };

        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmployeeNum = this.handleChangeEmployeeNum.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this); 
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    handleChangeEmployeeNum(event) {
        const value = event.target.value;
        this.setState({ employee_number: value });
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }, 
        };

        const response = await fetch(technicianUrl, fetchConfig);


        if (response.ok) {
            const newTech = await response.json();
            const cleared = {
                name: '',
                employee_number: '',
            };
            this.setState(cleared);
        }
    }

    // handleFieldChange = (event) =>{
    //     event.preventDefault();
    //     this.setState({
    //         ...this.state,
    //         [event.target.name]: event.target.value
    //     });
    // }

    // async componentDidMount() {
    //     const url = "http://localhost:3000/api/service/";

    //     const response = await fetch(url);
    //     console.log(response)

    //     if (response.ok) {
    //         const data = await response.json();
    //         this.setState({bins: data.bins});
    //     }
    // }

    

    render() {
        return (
            <>
            {/* <button
                className="btn btn-outline-light"
                onClick={(e) => {
                    e.preventDefault();
                    window.location.href="http://localhost:8000/service";
                }}
                >Back to Services Home
            // </button> */}
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Enter Technician</h1>
                            <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.handleChangeName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.employee_number} onChange={this.handleChangeEmployeeNum} placeholder="Employee Number" required type="text" name="employee_number" id="employee_num" className="form-control" />
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>
                            <button className="btn btn-outline-success">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}


export default ServiceTechnicianForm
