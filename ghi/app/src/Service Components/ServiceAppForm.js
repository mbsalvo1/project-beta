import React from 'react';


class ServiceAppForm extends React.Component {
    constructor() {
        super()
        this.state = {
            vin: '',
            customer: '',
            date_time: '',
            reason: '',
            technician: '',
            technicians: []
        };

        this.handleChangeVin = this.handleChangeVin.bind(this);
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
        this.handleChangeDateTime = this.handleChangeDateTime.bind(this);
        this.handleChangeReason = this.handleChangeReason.bind(this);
        this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this); 
    }


    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }
    
    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({ customer: value });
    }


    handleChangeDateTime(event) {
        const value = event.target.value;
        this.setState({ date_time: value });
    }
    
    handleChangeReason(event) {
        const value = event.target.value;
        this.setState({ reason: value });
    }
 
    
    handleChangeTechnician(event) {
        const value = event.target.value;
        this.setState({ technician: value });
    }



    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        delete data.technicians;

        const url = 'http://localhost:8080/api/appointments/';

        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },

        };
        const response = await fetch(url, fetchOptions);

        if (response.ok) {
            const cleared = {
                vin: '',
                customer: '',
                date_time: '',
                reason: '',
                technician: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({technicians: data.technicians});
        }
    }

    


    render() {
        return (
            <>

            {/* <button
                className="btn btn-outline-light"
                name="/services/"
                onClick={this.props.handleNavigate}
                >Back to Services Home
            </button> */}
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create Appointment</h1>
                            <form onSubmit={this.handleSubmit} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.vin} onChange={this.handleChangeVin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.customer} onChange={this.handleChangeCustomer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                                <label htmlFor="customer">Customer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.date_time} onChange={this.handleChangeDateTime} placeholder="Date Time" type="datetime-local" name="date_time" id="date_time" className="form-control" />
                                <label htmlFor="date_time">Date / Time</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.technician} onChange={this.handleChangeTechnician} required name="technician" id="technician" className="form-select" >
                                <option value="">Choose a technician</option>
                                {this.state.technicians.map(technician => {
                                    return (
                                    <option key={technician.id} value={technician.employee_number}>
                                        {technician.name}
                                    </option>
                                    );
                                })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="reason">Reason</label>
                                <textarea value={this.state.reason} onChange={this.handleChangeReason} rows="3" name="reason" id="reason" className="form-control"></textarea>
                            </div>
                            <button className="btn btn-outline-success">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

    
export default ServiceAppForm;



