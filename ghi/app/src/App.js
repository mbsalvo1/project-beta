import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SaleForm from './SalesComponents/SaleForm';
import SalesList from './SalesComponents/SalesHistory';
import SalesPersonForm from './SalesComponents/NewEmployee';
import NewCustomerForm from './SalesComponents/NewCustomer';
import SalesSearch from './SalesComponents/SalesSearch';
import ManufacturerList from './InventoryComponents/ManufacturerList';
import ModelList from './InventoryComponents/ModelList';
import AutoList from './InventoryComponents/AutomobileList';
import ManufacturerForm from './InventoryComponents/NewManufacturer';
import VehicleModelForm from './InventoryComponents/NewModel';
import NewAutoForm from './InventoryComponents/NewAutomobile';
import ServiceAppForm from './Service Components/ServiceAppForm';
import ServiceAppList from './Service Components/ServiceAppList';
import ServiceTechnicianForm from './Service Components/ServiceTechForm';
import InventorySearch from './InventoryComponents/InventorySearch';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/sales/new" element={<SaleForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/customer" element={<NewCustomerForm />} />
          <Route path="/sales/employee" element={<SalesPersonForm />} />
          <Route path="/sales/performance" element={<SalesSearch />} />

          <Route path="/sales/manufacturers" element={<ManufacturerList />} />
          <Route path="/sales/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/sales/models" element={<ModelList />} />
          <Route path="/sales/models/new" element={<VehicleModelForm />} />
          <Route path="/sales/automobiles" element={<AutoList />} />
          <Route path="/sales/automobiles/new" element={<NewAutoForm />} />
          <Route path="/sales/automobiles/search" element={<InventorySearch />} />

          
          <Route path="/service/appointments/new" element={<ServiceAppForm />} />
          <Route path="/service/appointments/" element={<ServiceAppList />} />
          <Route path="/service/technicians/new" element={<ServiceTechnicianForm />} />


        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
