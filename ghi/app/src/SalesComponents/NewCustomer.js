import React from 'react';

class NewCustomerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            address: '',
            phone_number: ''
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({address: value})
    }
    handlePhoneNumberChange(event) {
        const value = event.target.value;
        this.setState({phone_number: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log("submit data",data)

        const salesUrl = 'http://localhost:8090/api/sales/customer/';
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {

          const cleared = {
            name: '',
            address: '',
            phone_number: ''
          };
          this.setState(cleared);
        }
      }

    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create Customer</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange= {this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="Name" id="Name" className="form-control"/>
                  <label htmlFor="style">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleAddressChange} value={this.state.address} placeholder="Address" required type="text" name="Address" id="Address" className="form-control"/>
                  <label htmlFor="style">Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handlePhoneNumberChange} value={this.state.phone_number} placeholder="PhoneNumber" required type="text" name="PhoneNumber" id="PhoneNumber" className="form-control"/>
                  <label htmlFor="style">Phone Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default NewCustomerForm;
