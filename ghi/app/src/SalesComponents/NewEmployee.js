import React from 'react';

class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            employee_number: ''
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState({employee_number: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        const salesUrl = 'http://localhost:8090/api/sales/salesperson/';
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {

          const cleared = {
            name: '',
            employee_number: '',
          };
          this.setState(cleared);
        }
      }

    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create Salesman</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange= {this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="Name" id="Name" className="form-control"/>
                  <label htmlFor="style">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleEmployeeNumberChange} value={this.state.employee_number} placeholder="Employee" required type="text" name="Employee" id="Employee" className="form-control"/>
                  <label htmlFor="style">Employee Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default SalesPersonForm;
