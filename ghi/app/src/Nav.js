import { NavLink } from 'react-router-dom';
import { Dropdown } from 'react-bootstrap';
function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
      <div id="sales">
          <Dropdown>
              <Dropdown.Toggle variant="success" id="dropdown-basic">Sales</Dropdown.Toggle>
              <Dropdown.Menu>
                  <Dropdown.Item href="/sales">Sales List</Dropdown.Item>
                  <Dropdown.Item href="/sales/performance">Salesman Performance</Dropdown.Item>
                  <Dropdown.Item href="/sales/new">Create A Sale</Dropdown.Item>
                  <Dropdown.Item href="/sales/customer">Add a Customer</Dropdown.Item>
                  <Dropdown.Item href="/sales/employee">Add a Salesman</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
      </div>
      <div id="Inventory">
          <Dropdown>
              <Dropdown.Toggle variant="success" id="dropdown-basic">Inventory</Dropdown.Toggle>
              <Dropdown.Menu>
                  <Dropdown.Item href="/sales/automobiles">Inventory List</Dropdown.Item>
                  <Dropdown.Item href="/sales/automobiles/new">Add a Automobile</Dropdown.Item>
                  <Dropdown.Item href="/sales/automobiles/search">Search Inventory</Dropdown.Item>
                  <Dropdown.Item href="/sales/models">Model List</Dropdown.Item>
                  <Dropdown.Item href="/sales/models/new">Add a Model</Dropdown.Item>
                  <Dropdown.Item href="/sales/manufacturers">Manufacturer List</Dropdown.Item>
                  <Dropdown.Item href="/sales/manufacturers/new">Add a Manufacturer</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
      </div>
      <div id="service"></div>
      <div id="inventory"></div>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
